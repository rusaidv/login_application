using System.Linq;
using login_app.Models.Data;
using Microsoft.AspNetCore.Mvc;

namespace login_app.Controllers
{
    public class ValidationController : Controller
    {
        private LoginContext _db;

        public ValidationController(LoginContext db)
        {
            _db = db;
        }

        [AcceptVerbs("GET", "POST")]
        public bool ValidateEmail(string email)
        {
            return _db.Users.Any(u => u.Email == email);
        }
    }
}