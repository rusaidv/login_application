using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace login_app.ViewModels.Account
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Введите адрес электронной почты")]
        [Remote("ValidateEmail", "Validation", ErrorMessage ="Пользователь с такой почтой не зарегистрирован")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Введите пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public bool RememberMe { get; set; }
        public string ReturnUrl { get; set; }
    }
}