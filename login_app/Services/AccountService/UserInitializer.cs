using System.Threading.Tasks;
using login_app.Models;
using Microsoft.AspNetCore.Identity;


namespace login_app.Services.AccountService
{
    public class UserInitializer
    {
        public static async Task SeedUser(RoleManager<IdentityRole> _roleManager,UserManager<User> _userManager)
        {
            string email = "admin@admin.com";
            string password = "admin";
            
            var roles = new [] { "admin", "user" };
  
            foreach (var role in roles)
            {
                if (await _roleManager.FindByNameAsync(role) is null)
                    await _roleManager.CreateAsync(new IdentityRole(role));
            }
            if (await _userManager.FindByNameAsync(email) == null)
            {
                User admin = new User { Email = email, UserName = email };
                IdentityResult result = await _userManager.CreateAsync(admin, password);
                if (result.Succeeded)
                    await _userManager.AddToRoleAsync(admin, "admin");
            }
        }
    }
}