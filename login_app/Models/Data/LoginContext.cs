
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace login_app.Models.Data
{
    public class LoginContext : IdentityDbContext<User>
    {
        public DbSet<User> Users { get; set; }
        
        public LoginContext(DbContextOptions<LoginContext> options) : base(options)
        {
        }
    }
}